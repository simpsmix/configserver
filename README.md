Practice

+ configserver

###TAHAP PERTAMA###

1. Buat repo configserver - checkout ke local pc
2. Buat repo konfigurasi - checkout ke local pc
3. Buka start.spring.io
   isi group --> com.configserver
   isi artifact --> configserver
4. search for dependencies
   Config Server
5. Generate Project - ini akan langsung tergenerate otomatis source code framework spring boot.
6. Open project generate diatas dengan code editor.
7. Buka Main/java/com/configserver/configserver/ConfigServerConfiguration.java
8. Tambahkan annotation @EnableServerConfig untuk mengenali bahwa ini adalah aplikasi ConfigServer
9. Buka resources/application.properties
10. Isi properties dengan:
spring.application.name=configserver 
spring.cloud.config.server.git.uri=https://gitlab.com/simpsmix/konfigurasi.git
spring.cloud.config.server.bootstrap=true
spring.cloud.config.server.git.searchPaths={application}

note. 
line 1: nama aplikasinya
line 2: alamat git yang berisi konfigurasi (bukan source code utama)
line 3: membuat tampilan menjadi rapih
line 4: agar mudah dicari oleh server

11. Ubah nama application.properties menjadi bootstrap.properties (untuk menerapkan searchPaths)
12. Commit - Push

###TAHAP KEDUA###

1. Pada repo konfigurasi, buat file application.properties
Bisa dengan vim application.properties (pada git bash)
2. Isi spring.jackson.serialization.indent_output=true , save.
3. Buat folder baru misal namanya wallet dan buat file application.properties
4. Isi 

spring.datasource.url=jdbc:mysql://localhost/walletdb
spring.datasource.username={cipher}254d06128b4e28f22f98074ccfe4f5f74bf15f08968e99858f4cabb4101d10ca
spring.datasource.password={cipher}ba7f9d31a759e70620fca4f27d917250e7f53031709db45ca1b0da891b1f0ed2


line 1: host db
line 2: username encrypt
line 3: password encrypt

5. Commit - push
6. Jalankan aplikasi : mvn spring-boot:run
7. Open browser: localhost:8080/wallet/default